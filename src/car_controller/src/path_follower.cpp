// Some early code to peform trajectory following 
// Not functional yet. Was build with a goal message
// being a sequence of waypoints

#include <ros/ros.h>
#include <angles/angles.h>
#include <actionlib/server/simple_action_server.h>
#include <path_follower/FollowPathAction.h>

class PathFollowing
{
public:
  PathFollowing(std::string name) :
    m_as(m_nh, name, false)
  {
  }
  
  ~PathFollowing() {
    // do nothing
  }

protected:
  void executeCB(const path_follower::FollowPathGoalConstPtr &goal) {
    bool completed = false;
    for (int i=0; i<goal->wpts.size(); i++) {
      if ( m_as.isPreemptRequested() || !ros::ok() ) {
        m_as.setPreempted();
        ROS_INFO("Current path aborted at waypoint # %d!", i+1);
        break;
      }
      geometry_msgs::Point cur_wpt = goal->wpts[i];
      setGoal(i, goal);
      // while ( !goalAchieved() ) {
      // }
      // if ( !success ) {
      //   ROS_INFO("Failed to reach waypoint # %d!", m_activeWptIndex);
      //   m_as.setAborted();
      //   break;
      // }
    }
  }
  
  void setGoal(int i, const path_follower::FollowPathGoalConstPtr &goal) {

    m_goalAchieved = false;
    geometry_msgs::Point cur_wpt = goal->wpts[i];

    double target_angle;
    double target_speed;
    double target_x = cur_wpt.x;
    double target_y = cur_wpt.y;

    if ( i < goal->wpts.size()-1 ) {
      geometry_msgs::Point next_wpt = goal->wpts[i+1];
      double dx2 = next_wpt.x-cur_wpt.x;
      double dy2 = next_wpt.y-cur_wpt.y;
      double angle2 = atan2(dx2, dy2);
      if ( i > 1 ) {
        geometry_msgs::Point prev_wpt = goal->wpts[i-1];
        double dx1 = cur_wpt.x-prev_wpt.x;
        double dy1 = cur_wpt.y-prev_wpt.y;
        double angle1 = atan2(dx1, dy1);
        target_angle = (
          angles::normalize_angle_positive(angle1)
          +angles::normalize_angle_positive(angle2)
          )/2.0;
      }
      else {
        target_angle = angle2;
      }
      target_speed = NOMINAL_SPEED;
    }
    else {
      geometry_msgs::Point prev_wpt = goal->wpts[i-1];
      double dx1 = cur_wpt.x-prev_wpt.x;
      double dy1 = cur_wpt.y-prev_wpt.y;
      target_angle = atan2(dx1, dy1);
      target_speed = 0;
    }
    ROS_INFO("Set goal to waypoint # %d -> (%f, %f)", i+1, cur_wpt.x, cur_wpt.y);
  }

/*
  void setGoal(geometry_msgs::Point wpt) {
  ROS_INFO("Set goal to waypoint # %d -> (%f, %f)", m_activeWptIndex+1, wpt.x, wpt.y)  }
*/

  bool driveToGoal() {
    // This function need to be called on receiving new position
    // and then update the target speed / curvature
    float distance = 100;
    while ( ros::ok() && distance > TOLERANCE ) {
      distance -= 1;
    }
  }

  ros::NodeHandle m_nh;
  actionlib::SimpleActionServer<path_follower::FollowPathAction> m_as;
  path_follower::FollowPathFeedback m_feedback;
  path_follower::FollowPathResult m_result;
  int m_activeWptIndex;
  
  // should mutex protect these variables
  bool m_goalAchieved;

  static const double TOLERANCE = 0.1;
  static const double NOMINAL_SPEED = 1; 

};

int main(int argc, char** argv)
{
  ros::init(argc, argv, "path_follower");

  PathFollowing("follow_path_action");

  ros::spin();

  return 0;
}
