#include <ros/ros.h>

#include <car_messages/Drive.h>

int main(int argc, char **argv)
{
  if ( argc != 3 ) {
    ROS_INFO("usage: %s speed curvature", argv[0]);
    return 1;
  }

  ros::init(argc, argv, "send_drive_client");

  ros::NodeHandle nh;
  ros::ServiceClient client = nh.serviceClient<car_messages::Drive>("/drive_service");

  car_messages::Drive srv;
  srv.request.speed = atof(argv[1]);
  srv.request.curvature = atof(argv[2]);

  if ( client.call(srv) ) {
    ROS_INFO("reset response = %s", srv.response.valid?"valid":"invalid parameters");
  }
  else {
    ROS_ERROR("Failed to call drive service!");
    return 1;
  }
  return 0;
}
