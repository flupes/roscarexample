#include <signal.h>

#include <ros/ros.h>

#include <actionlib/client/simple_action_client.h>
#include <car_messages/SetWaypointAction.h>

class CommandGoal {

public:
  CommandGoal(double x, double y, double th) :
    m_nh(), m_client("car_controller", true)
  {
    m_goal.wpt.x = x;
    m_goal.wpt.y = y;
    m_goal.wpt.theta = th;
    ROS_INFO("waiting for the server car_controller");
    m_client.waitForServer();
    ROS_INFO("sending goal SetWaypoint to car_controller");
    m_client.sendGoal(m_goal,
                      boost::bind(&CommandGoal::doneCallback, this, _1, _2),
                      NULL,
                      boost::bind(&CommandGoal::feedbackCallback, this, _1) );
    ROS_INFO("waiting for completion...");
    ros::spin();
  }

  void feedbackCallback(const car_messages::SetWaypointFeedbackConstPtr& feedback) {
    ROS_INFO("SetWaypoint feedback: distance=%f, turn_angle_to_goal=%f, final_heading_error=%f",
             feedback->distance_to_goal,
             feedback->turn_angle_to_goal*180.0/M_PI,
             feedback->final_heading_error*180.0/M_PI);
  }

  void doneCallback(const actionlib::SimpleClientGoalState& state,
                    const car_messages::SetWaypointResultConstPtr& result) {
    ROS_INFO("SetWaypoint action completed. Result = %s",
             result->success?"success":"failure");
    ros::shutdown();
  }

protected:
  ros::NodeHandle m_nh;
  actionlib::SimpleActionClient<car_messages::SetWaypointAction> m_client;
  car_messages::SetWaypointGoal m_goal;
};

void quit(int sig)
{
  ros::shutdown();
  exit(0);
} 

int main(int argc, char **argv)
{
  if ( argc != 4 ) {
    ROS_INFO("usage: %s x(m) y(m) theta(degrees)", argv[0]);
    return 1;
  }

  ros::init(argc, argv, "send_goal");
  signal(SIGINT,quit);

  double x = atof(argv[1]);
  double y = atof(argv[2]);
  double theta = atof(argv[3])*M_PI/180.0;

  CommandGoal action(x, y, theta);

  return 0;
}
