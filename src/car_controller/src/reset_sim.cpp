#include <ros/ros.h>

#include <car_messages/Reset.h>

// Reset the robot speed/curvature and position it at the given location
// Warning: theta angle given on the command line in degrees!

int main(int argc, char **argv)
{
  if ( argc != 4 ) {
    ROS_INFO("usage: %s x(m) y(m) theta(degrees)", argv[0]);
    return 1;
  }

  ros::init(argc, argv, "reset_simulator_client");

  ros::NodeHandle nh;
  ros::ServiceClient client = nh.serviceClient<car_messages::Reset>("/reset_service");

  car_messages::Reset srv;
  srv.request.wpt.x = atof(argv[1]);
  srv.request.wpt.y = atof(argv[2]);
  srv.request.wpt.theta = atof(argv[3])*M_PI/180.0;

  if ( client.call(srv) ) {
    ROS_INFO("reset response = %s", srv.response.completed?"completed":"failed");
  }
  else {
    ROS_ERROR("Failed to call reset service!");
    return 1;
  }
  return 0;
}
