#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <car_messages/SetWaypointAction.h>
#include <car_messages/Drive.h>

class CarController
{
public:
  CarController(std::string name) :
    m_nh(),
    m_as(m_nh, name, false)
  {
    // Because of the template form tof the param methods, 
    // the constants defined inline are not recognized...
    // m_nh.param("k_alph", m_kalph, CarController::DEFAULT_K_ALPH);
    // m_nh.param("k_beta", m_kbeta, DEFAULT_K_BETA);
    // m_nh.param("k_dist", m_kdist, DEFAULT_K_DIST);
    m_nh.param("k_alph", m_kalph, 1.0);
    m_nh.param("k_beta", m_kbeta, -0.2);
    m_nh.param("k_dist", m_kdist, 0.4);


    m_as.registerGoalCallback(boost::bind(&CarController::goalCallback, this));
    m_as.registerPreemptCallback(boost::bind(&CarController::preemptCallback, this));
    ROS_INFO("CarController is waiting for service /drive_service");
    ros::service::waitForService("/drive_service");
    m_client = m_nh.serviceClient<car_messages::Drive>("/drive_service");
    ROS_INFO("Got the service, launching the action server for /set_waypoint");
    m_as.start();

    m_sub = m_nh.subscribe("/car_pose", 1, &CarController::poseCallback, this);
    
    m_active = false;
    ROS_INFO("CarController done with initializations");
  }
  
  ~CarController() {
    // do nothing
  }

protected:
  void goalCallback() {
    ROS_INFO("goalCallback");
    m_goalWpt = m_as.acceptNewGoal()->wpt;
    m_active = true;
    m_startGoal = ros::Time::now();
  }


  void preemptCallback() {
    ROS_INFO("preemptCallback");
    m_active = true;
    m_startGoal = ros::Time::now();  
    m_as.setPreempted();
  }

  void poseCallback(const geometry_msgs::Pose2D::ConstPtr& msg) {
    //ROS_INFO("received new pose...");
    m_carPose.x = msg->x;
    m_carPose.y = msg->y;
    m_carPose.theta = msg->theta;
    driveToWaypoint();
  }

  bool driveToWaypoint(double finalSpeed=0) {

    car_messages::SetWaypointResult result;

    ros::Time current = ros::Time::now();
    if ( m_active && ( (current.toSec() - m_startGoal.toSec()) > TIMEOUT ) ) {
      result.success = false;
      m_as.setSucceeded(result);
      m_active = false;
      return false;
    }
    
    if ( m_active ) {
      
      double dx = m_goalWpt.x - m_carPose.x;
      double dy = m_goalWpt.y - m_carPose.y;
      double distance = sqrt(dx*dx+dy*dy);

      car_messages::Drive drive;
      if ( distance < TOLERANCE ) {
        result.success = true;
        m_as.setSucceeded(result);
        m_active = false;
        drive.request.speed = 0;
        drive.request.curvature = 0;
        m_client.call(drive);       // should check for errors, see example below
        return true;
      }
      
      double tau = atan2(dy, dx);
      double alpha = tau - m_carPose.theta;
      double beta = m_goalWpt.theta - tau;

      ROS_DEBUG("x=%f, y=%f, th=%f, dx=%f, dy=%f, dist=%f, beta=%f, alpha=%f", 
               m_carPose.x, m_carPose.y, m_carPose.theta,
               dx, dy, distance, beta, alpha);
      drive.request.speed = finalSpeed + m_kdist*distance;
      if ( drive.request.speed > NOMINAL_SPEED ) drive.request.speed = NOMINAL_SPEED;
      double angularVel = m_kalph * alpha + m_kbeta * beta;
      drive.request.curvature = angularVel / drive.request.speed;
      if ( drive.request.curvature > 0.25 ) drive.request.curvature = 0.25;
      if ( drive.request.curvature < -0.25 ) drive.request.curvature = -0.25;
      ROS_DEBUG("  -> speed = %f / angularVel = %f (curvature=%f)",
               drive.request.speed, angularVel, drive.request.curvature);
      if ( ! m_client.call(drive) ) {
        ROS_ERROR("CarController could not get service request on /drive_service!");
      }

      car_messages::SetWaypointFeedback feedback;
      feedback.distance_to_goal = distance;
      feedback.turn_angle_to_goal = alpha;
      feedback.final_heading_error = beta;
      m_as.publishFeedback(feedback);
    }
    return false;
  }

  ros::NodeHandle m_nh;
  actionlib::SimpleActionServer<car_messages::SetWaypointAction> m_as;
  ros::ServiceClient m_client;
  car_messages::SetWaypointFeedback m_feedback;
  car_messages::SetWaypointResult m_result;
  ros::Subscriber m_sub;

  geometry_msgs::Pose2D m_carPose; 
  geometry_msgs::Pose2D m_goalWpt;
  ros::Time m_startGoal;
  bool m_active;

  double m_kdist;
  double m_kalph;
  double m_kbeta;

  static const double DEFAULT_K_ALPH = 0.8;
  static const double DEFAULT_K_BETA = -0.4;
  static const double DEFAULT_K_DIST = 1.0;

  static const double TOLERANCE = 0.3;
  static const double NOMINAL_SPEED = 2; 
  static const int TIMEOUT = 60;

};

int main(int argc, char** argv)
{
  ros::init(argc, argv, "car_controller");

  CarController controller("car_controller");

  ros::spin();

  return 0;
}
