#!/usr/bin/env python

import roslib; roslib.load_manifest('car_controller')
import rospy;

import actionlib

import geometry_msgs.msg

import car_messages.msg

import sys

def set_waypoint_client(x, y, theta):
    client = actionlib.SimpleActionClient('/car_controller', car_messages.msg.SetWaypointAction)
    
    print "waiting for server..."
    client.wait_for_server()

    wpt = geometry_msgs.msg.Pose2D(x,y,theta)
    goal = car_messages.msg.SetWaypointGoal(wpt)

    print "sending goal..."
    client.send_goal(goal)

    print "wait_for_result..."
    client.wait_for_result()

if __name__ == '__main__':
    if len(sys.argv) < 4:
        print "Usage:",sys.argv[0], " x_pos y_pos theta"
    else:
        try:
            x_pos = float(sys.argv[1])
            y_pos = float(sys.argv[2])
            theta = float(sys.argv[3])
            
            rospy.init_node('set_waypoint_client')
            print "command car to (",x_pos,",",y_pos,",",theta,")"
            result = set_waypoint_client(x_pos, y_pos, theta)
            print "result = ", result.success
        except rospy.ROSInterruptException:
            print "action interrupted before completion!"
            
