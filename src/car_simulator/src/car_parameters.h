#ifndef CAR_LIMITS_H
#define CAR_LIMITS_H

class CarLimits {
public:
  static const double MAX_SPEED;
  static const double MAX_CURVATURE;
};

#endif
