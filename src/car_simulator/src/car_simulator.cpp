#include <ros/ros.h>
#include <car_messages/Drive.h>
#include <car_messages/Reset.h>

#include <geometry_msgs/Pose2D.h>
#include <tf/transform_broadcaster.h>

#include "car_parameters.h"

class CarSimulator {

public:
  CarSimulator() :
    m_nh()
  {
    m_driveServer = m_nh.advertiseService("drive_service", 
                                     &CarSimulator::driveCallback, this);
    m_resetServer = m_nh.advertiseService("reset_service", 
                                     &CarSimulator::resetCallback, this);
    period = 1.0 / (double)RATE;
    m_speed = 0.0;
    m_curvature = 0.0;
    m_wheelRotation = 0;
    m_pub = m_nh.advertise<geometry_msgs::Pose2D>("/car_pose", 128);
    m_counter = 0;
  }

  void update() {

    double ldx, ldy;    // local dx an dy over the period
    double cdx, cdy;    // world dx and dy
    double dth;         // angle variation over the period

    if ( m_speed > 1E-3 ) {
      // not stopped (avoid rounding error)
      if ( fabs(m_curvature) > 1E-3 ) {
        // driving on a arc circle
        double radius = 1 / m_curvature;
        dth = m_speed*period/radius;
        ldx = sin(dth)*radius;
        ldy = (1-cos(dth))*radius;

        // ROS_DEBUG("radius=%f", radius);
        // Simplification, just to see something steering action      
        // HALF_BASE should come from the URDF model
        m_steerAngle = atan2(WHEEL_BASE/2.0, abs(radius)-WHEEL_TRACK/2.0);
        if ( radius < 0 ) {
          m_steerAngle = -m_steerAngle;
        }
      }
      else {
        // driving straight!
        ldy = 0;
        ldx = m_speed * period;
        dth = 0.0;
        m_steerAngle = 0;
      }
      // Simplification: vehicle rotates around its center rather than the rear train!
      cdx = cos(m_pose.theta)*ldx - sin(m_pose.theta)*ldy;
      cdy = sin(m_pose.theta)*ldx + cos(m_pose.theta)*ldy;
      m_pose.x += cdx;
      m_pose.y += cdy;
      m_pose.theta += dth;
      if ( m_pose.theta < M_PI ) m_pose.theta += 2.0*M_PI;
      if ( m_pose.theta > M_PI ) m_pose.theta -= 2.0*M_PI;
    }
    
    // Publish the pose as a normal message (for the controller)
    // Only publish at a quarter rate (just for fun)
    if ( m_counter % 4 == 0 ) {
      m_pub.publish(m_pose);
    }
    m_counter++;

    // Simplification: all wheel spin at the same rate!
    // Also, WHEEL_RADIUS should be queried from the URDF model
    m_wheelRotation += m_speed * period / WHEEL_RADIUS;

    m_world2car.setOrigin( tf::Vector3(m_pose.x, m_pose.y, 0.0) );
    m_world2car.setRotation( tf::Quaternion(tf::Vector3(0,0,1), m_pose.theta) );
    m_br.sendTransform( tf::StampedTransform( m_world2car, ros::Time::now(), "world", "origin") );

    m_wheelSteer.setOrigin( tf::Vector3(0, 0, 0) );
    m_wheelSteer.setRotation( tf::Quaternion(tf::Vector3(0,0,1), m_steerAngle) );
    m_br.sendTransform( tf::StampedTransform( m_wheelSteer, ros::Time::now(), "left_front_hub", "left_steer_arm") );
    m_br.sendTransform( tf::StampedTransform( m_wheelSteer, ros::Time::now(), "right_front_hub", "right_steer_arm") );

    m_wheelSpin.setOrigin( tf::Vector3(0, 0, 0) );
    m_wheelSpin.setRotation( tf::Quaternion(tf::Vector3(0,0,1), m_wheelRotation) );
    m_br.sendTransform( tf::StampedTransform( m_wheelSpin, ros::Time::now(), "left_front_axis", "left_front_wheel") );
    m_br.sendTransform( tf::StampedTransform( m_wheelSpin, ros::Time::now(), "right_front_axis", "right_front_wheel") );
    m_br.sendTransform( tf::StampedTransform( m_wheelSpin, ros::Time::now(), "left_rear_hub", "left_rear_wheel") );
    m_br.sendTransform( tf::StampedTransform( m_wheelSpin, ros::Time::now(), "right_rear_hub", "right_rear_wheel") );
    
  }

  static const int RATE = 10; // 10 Hz
  static const double WHEEL_RADIUS = 0.3;
  static const double WHEEL_BASE = 3.2;
  static const double WHEEL_TRACK = 2.4;

protected:

  bool driveCallback(car_messages::Drive::Request& req, 
                     car_messages::Drive::Response& res) {
    // should lock speed and curvature while updating...
    m_speed = req.speed;
    m_curvature = req.curvature;
    ROS_INFO("Got new request: speed=%f / curvature=%f", m_speed, m_curvature);
    if ( abs(m_speed) <= CarLimits::MAX_SPEED && abs(m_curvature) <= CarLimits::MAX_CURVATURE ) {
      res.valid = true;
    }
    else {
      if ( m_speed > 0 ) m_speed = CarLimits::MAX_SPEED;
      if ( m_speed < 0 ) m_speed = -CarLimits::MAX_SPEED;
      if ( m_curvature > 0 ) m_curvature = CarLimits::MAX_CURVATURE;
      if ( m_curvature < 0 ) m_curvature = -CarLimits::MAX_CURVATURE;
      res.valid = false;
    }
    return true;
  }

  bool resetCallback(car_messages::Reset::Request& req, 
                     car_messages::Reset::Response& res) {
    m_speed = 0;
    m_curvature = 0;
    m_pose.x = req.wpt.x;
    m_pose.y = req.wpt.y;
    m_pose.theta = req.wpt.theta;
    ROS_INFO("Reset to: x=%f, y=%f, theta=%f, speed=%f, curvature=%f",
             m_pose.x, m_pose.y, m_pose.theta, m_speed, m_curvature);
    res.completed = true;
    return true;
  }
  
  ros::ServiceServer m_driveServer;
  ros::ServiceServer m_resetServer;
  ros::Publisher m_pub;
  ros::NodeHandle m_nh;
  tf::TransformBroadcaster m_br;
  tf::Transform m_world2car;
  tf::Transform m_wheelSpin;
  tf::Transform m_wheelSteer;

  unsigned short m_counter;
  double m_wheelRotation;
  double m_steerAngle;
  double m_speed;
  double m_curvature;
  double period;
  geometry_msgs::Pose2D m_pose;
};

int main(int argc, char** argv) 
{
  ros::init(argc, argv, "car_simulator");

  CarSimulator sim;

  ros::Rate loopRate(CarSimulator::RATE);

  while ( ros::ok() ) {

    ros::spinOnce();

    sim.update();
    
    loopRate.sleep();

  }

}
