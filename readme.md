# Quick Start

## Checkout and build
> source /opt/ros/hydro/setup.bash
>
> git clone https://bitbucket.org/flupes/roscarexample.git
>
> cd roscarexample
>
> catkin_make --pkg car_messages
>
> catkin_make
>
> source devel/setup.bash

Note: the double `catkin_make` is only required the first time, for which,
somehow the package dependecies is not respected.

## Run
Use two shells in which you have sourced
`your_working_dir/roscarexample/devel/setup.bash`:
> source devel/setup.bash

### Launch 4 keys nodes (simulator, controller, tf_pub and rviz)
> roslaunch car_controller car_demo.launch 

### Send waypoints to the car
> rosrun car_controller command_goal 12 6 45

### Reset the sim
> rosrun car_controller reset_sim 0 0 0

### Send low level commands to the car
> rosrun car_controller send_drive 5 0.2

## More
More notes are in the `readme.org` file at the top level of the repo.
